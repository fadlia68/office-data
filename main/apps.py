from django.apps import AppConfig
from office.db import Database
from os import getenv


class MainConfig(AppConfig):
    name = 'main'

    def ready(self):
        if getenv("DATABASE_URL") == None:
            print("Configuring Main App")
        else:
            db = Database(schema="office")
            # create schema
            db.close()
