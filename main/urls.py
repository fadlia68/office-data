from django.urls import path

from . import views

app_name = 'main'

urlpatterns = [
    path('', views.home, name='home'),
    path('pengalaman', views.pengalaman, name='pengalaman'),
    path('penggunaJasa', views.penggunaJasa, name='penggunaJasa'),
    path('pekerjaan', views.pekerjaan, name='pekerjaan'),
    path('layanan', views.layanan, name='layanan'),
    path('platform', views.platform, name='platform'),
]
