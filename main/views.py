from django.http import HttpResponseRedirect
from django.shortcuts import render
from office.db import Database


def home(request):
    return render(request, 'main/home.html')


def penggunaJasa(request):

    database = Database(schema='office')

    if request.method == 'GET':
        res = database.query('''
            SELECT * 
            FROM PENGGUNA_JASA
        ''')
        response = {
            'result': [
                {"id": pj[0],
                 "nama": pj[1]}
                for pj in res
            ]
        }
        return render(request, 'main/penggunaJasa.html', response)

    else:
        nama = request.POST.get('nama')
        res = database.query('''
            SELECT id
            FROM PENGGUNA_JASA
            ORDER BY id DESC
            LIMIT 1
        ''')
        if len(res) == 0:
            database.query(f'''
                INSERT INTO PENGGUNA_JASA VALUES 
                (1, '{nama}')
            ''')
        else:
            database.query(f'''
                INSERT INTO PENGGUNA_JASA VALUES 
                ({res[0][0] + 1}, '{nama}')
            ''')
        return HttpResponseRedirect('/penggunaJasa')


def pekerjaan(request):

    database = Database(schema='office')

    if request.method == 'GET':
        res = database.query('''
            SELECT * 
            FROM PEKERJAAN
        ''')
        response = {
            'result': [
                {"id": pj[0],
                 "nama": pj[1]}
                for pj in res
            ]
        }
        return render(request, 'main/pekerjaan.html', response)

    else:
        nama = request.POST.get('nama')
        res = database.query('''
            SELECT id
            FROM PEKERJAAN
            ORDER BY id DESC
            LIMIT 1
        ''')
        if len(res) == 0:
            database.query(f'''
                INSERT INTO PEKERJAAN VALUES 
                (1, '{nama}')
            ''')
        else:
            database.query(f'''
                INSERT INTO PEKERJAAN VALUES 
                ({res[0][0] + 1}, '{nama}')
            ''')
        return HttpResponseRedirect('/pekerjaan')


def layanan(request):

    database = Database(schema='office')

    if request.method == 'GET':
        res = database.query('''
            SELECT * 
            FROM LAYANAN
        ''')
        response = {
            'result': [
                {"id": pj[0],
                 "nama": pj[1]}
                for pj in res
            ]
        }
        return render(request, 'main/layanan.html', response)

    else:
        nama = request.POST.get('nama')
        res = database.query('''
            SELECT id
            FROM LAYANAN
            ORDER BY id DESC
            LIMIT 1
        ''')
        if len(res) == 0:
            database.query(f'''
                INSERT INTO LAYANAN VALUES 
                (1, '{nama}')
            ''')
        else:
            database.query(f'''
                INSERT INTO LAYANAN VALUES 
                ({res[0][0] + 1}, '{nama}')
            ''')
        return HttpResponseRedirect('/layanan')


def platform(request):

    database = Database(schema='office')

    if request.method == 'GET':
        res = database.query('''
            SELECT * 
            FROM PLATFORM
        ''')
        response = {
            'result': [
                {"id": pj[0],
                 "nama": pj[1]}
                for pj in res
            ]
        }
        return render(request, 'main/platform.html', response)

    else:
        nama = request.POST.get('nama')
        res = database.query('''
            SELECT id
            FROM PLATFORM
            ORDER BY id DESC
            LIMIT 1
        ''')
        if len(res) == 0:
            database.query(f'''
                INSERT INTO PLATFORM VALUES 
                (1, '{nama}')
            ''')
        else:
            database.query(f'''
                INSERT INTO PLATFORM VALUES 
                ({res[0][0] + 1}, '{nama}')
            ''')
        return HttpResponseRedirect('/platform')


def pengalaman(request):

    database = Database(schema='office')

    if request.method == 'GET':

        response = {}
        res = database.query('''
            SELECT nama
            FROM PENGGUNA_JASA
        ''')
        response['penggunaJasa'] = [pj[0] for pj in res]
        res = database.query('''
            SELECT nama
            FROM PEKERJAAN
        ''')
        response['pekerjaan'] = [p[0] for p in res]
        res = database.query('''
            SELECT nama
            FROM LAYANAN
        ''')
        response['layanan'] = [l[0] for l in res]
        res = database.query('''
            SELECT nama
            FROM PLATFORM
        ''')
        response['platform'] = [p[0] for p in res]
        response['year'] = range(2020, 2010, -1)

        res = database.query('''
            SELECT *
            FROM PENGALAMAN
        ''')

        response['pengalaman'] = [
            {'tahun': p[1], 'pj': p[2], 'pekerjaan': p[3], 'nilai': p[4], 'layanan': p[5],
             'platform': p[8], 'periode': p[6].strftime("%d %b %Y") + ' - ' + p[7].strftime("%d %b %Y")}
            for p in res
        ]

        return render(request, 'main/pengalaman.html', response)

    else:

        dct = dict(request.POST.lists())

        tahun = dct.get('tahun')[0]
        penggunaJasa = dct.get('penggunaJasa')[0]
        pekerjaan = dct.get('pekerjaan')[0]
        nilai = dct.get('nilai')[0]
        layanan = dct.get('layanan')[0]
        platforms = dct.get('platform')
        periode_start = dct.get('ps')[0]
        periode_end = dct.get('pe')[0]

        res = database.query('''
            SELECT id
            FROM PENGALAMAN
            ORDER BY id DESC
            LIMIT 1
        ''')

        platforms = ', '.join(platforms)
        if len(res) == 0:
            database.query(f'''
                INSERT INTO PENGALAMAN VALUES 
                (1, '{tahun}', '{penggunaJasa}', '{pekerjaan}', {nilai}, '{layanan}', '{periode_start}', '{periode_end}', '{platforms}')
            ''')
        else:
            database.query(f'''
                INSERT INTO PENGALAMAN VALUES 
                ({res[0][0] + 1}, '{tahun}', '{penggunaJasa}', '{pekerjaan}', {nilai}, '{layanan}', '{periode_start}', '{periode_end}', '{platforms}')
            ''')

        return HttpResponseRedirect('/pengalaman')
